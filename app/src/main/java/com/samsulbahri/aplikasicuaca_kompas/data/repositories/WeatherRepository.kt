package com.samsulbahri.aplikasicuaca_kompas.data.repositories

import com.samsulbahri.aplikasicuaca_kompas.BuildConfig
import com.samsulbahri.aplikasicuaca_kompas.data.database.AppDatabase
import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.*
import com.samsulbahri.aplikasicuaca_kompas.data.network.WeatherApi
import com.samsulbahri.aplikasicuaca_kompas.data.network.responses.WeatherResponse
import com.samsulbahri.aplikasicuaca_kompas.data.network.ApiRequest

class WeatherRepository(
    private val api: WeatherApi,
    private val db: AppDatabase
) : ApiRequest() {

    suspend fun dataWeather(lat: String, long: String): WeatherResponse {

        return apiRequest {api.getWeather(lat, long, BuildConfig.APP_ID, "minutely,daily")}

    }

    suspend fun saveBase(base: Base) = db.getBaseDao().upsert(base)

    fun getBase() = db.getBaseDao().getBaseData()

    suspend fun deleteBase() = db.getBaseDao().deleteAll()

}