package com.samsulbahri.aplikasicuaca_kompas.data.database.entities

data class Sys(
    var country: String? = null,
    var sunrise: String? = null,
    var sunset: String? = null
)
