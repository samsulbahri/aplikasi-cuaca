package com.samsulbahri.aplikasicuaca_kompas.data.network.responses

import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.MainInfo
import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.Sys
import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.Weather
import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.Wind

data class WeatherResponse(
    val id: Int?,
    val dt: String?,
    val name: String?,
    val weather: List<Weather?>,
    val main: MainInfo?,
    val wind: Wind?,
    val sys: Sys?
)