package com.samsulbahri.aplikasicuaca_kompas.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

const val CURRENT_BASE_ID = 0

@Entity
data class Base (
     @PrimaryKey(autoGenerate = false)
     var uid: Int? = CURRENT_BASE_ID,
     var date_time: String? = null,
     var name: String? = null,
     var temp: String? = null,
     var temp_min: String? = null,
     var temp_max: String? = null,
     var pressure: String? = null,
     var humidity: String? = null,
     var country: String? = null,
     var sunrise: String? = null,
     var sunset: String? = null,
     var main: String? = null,
     var speed: String? = null

)