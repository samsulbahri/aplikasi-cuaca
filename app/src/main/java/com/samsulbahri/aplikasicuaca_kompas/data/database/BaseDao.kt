package com.samsulbahri.aplikasicuaca_kompas.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.Base
import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.CURRENT_BASE_ID

@Dao
interface BaseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(base: Base) : Long

    @Query("SELECT * FROM Base")
    fun getBaseData() : LiveData<Base>

    @Query("DELETE FROM Base")
    suspend fun deleteAll()

}