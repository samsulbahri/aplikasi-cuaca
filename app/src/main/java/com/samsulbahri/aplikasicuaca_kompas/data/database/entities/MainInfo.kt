package com.samsulbahri.aplikasicuaca_kompas.data.database.entities

data class MainInfo(
    var temp: String? = null,
    var temp_min: String? = null,
    var temp_max: String? = null,
    var date_time: String? = null,
    var pressure: String? = null,
    var humidity: String? = null
)