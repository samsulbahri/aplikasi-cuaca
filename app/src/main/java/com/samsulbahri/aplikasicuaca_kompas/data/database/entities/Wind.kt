package com.samsulbahri.aplikasicuaca_kompas.data.database.entities

data class Wind(
    var speed: String? = null
)
