package com.samsulbahri.aplikasicuaca_kompas.data.database.entities

data class Weather(
    var main: String
)