package com.samsulbahri.aplikasicuaca_kompas.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.samsulbahri.aplikasicuaca_kompas.data.repositories.WeatherRepository

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory(
    private val repository: WeatherRepository
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(repository) as T
    }

}