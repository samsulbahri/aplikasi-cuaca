package com.samsulbahri.aplikasicuaca_kompas.ui.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.Base
import com.samsulbahri.aplikasicuaca_kompas.data.repositories.WeatherRepository
import com.samsulbahri.aplikasicuaca_kompas.ui.view.MainListener
import com.samsulbahri.aplikasicuaca_kompas.utils.ApiException
import com.samsulbahri.aplikasicuaca_kompas.utils.Coroutines
import com.samsulbahri.aplikasicuaca_kompas.utils.NoInternetException

class MainViewModel(
    private val repository: WeatherRepository
) : ViewModel() {

//    Balikpapan
//    var lat: String = "-1.2435946256987918"
//    var long: String = "116.84964156473943"

//    Jakarta
    var lat: String = "-6.218820311863041"
    var long: String = "106.84290535367667"


    var mainListener: MainListener? = null

    fun getDataBaseWeather() = repository.getBase()

    fun onDataLoad(isRefresh: Boolean) {
        mainListener?.onStarted()

        Coroutines.main {
            try {
                if(isRefresh) {
                    Log.d("CheckRefresh", "true")
                    repository.deleteBase()
                }

                val response = repository.dataWeather(lat, long)

                response.name?.let {
                    val mainInfo = response.main
                    val sys = response.sys
                    val weather = response.weather.last()
                    val wind = response.wind
                    val base = Base(response.id, response.dt, it, mainInfo!!.temp, mainInfo.temp_min, mainInfo.temp_max, mainInfo.pressure, mainInfo.humidity, sys!!.country, sys.sunrise, sys.sunset, weather!!.main, wind!!.speed)

                    mainListener?.onSuccess(base)

                    repository.saveBase(base)

                    return@main
                }
            } catch (e: ApiException) {
                mainListener?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                mainListener?.onNoInternet("Pastikan anda memiliki koneksi Internet untuk mendapatkan data terbaru", isRefresh)
            }
        }
    }

}