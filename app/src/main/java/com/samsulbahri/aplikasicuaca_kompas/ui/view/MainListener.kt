package com.samsulbahri.aplikasicuaca_kompas.ui.view

import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.*

interface MainListener {
    fun onStarted()
    fun onSuccess(base: Base)
    fun onFailure(message: String)
    fun onNoInternet(message: String, isRefresh: Boolean)
}