package com.samsulbahri.aplikasicuaca_kompas.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import com.samsulbahri.aplikasicuaca_kompas.R
import com.samsulbahri.aplikasicuaca_kompas.data.database.AppDatabase
import com.samsulbahri.aplikasicuaca_kompas.data.database.entities.*
import com.samsulbahri.aplikasicuaca_kompas.data.network.NetworkConnectionInterceptor
import com.samsulbahri.aplikasicuaca_kompas.data.network.WeatherApi
import com.samsulbahri.aplikasicuaca_kompas.data.repositories.WeatherRepository
import com.samsulbahri.aplikasicuaca_kompas.databinding.ActivityMainBinding
import com.samsulbahri.aplikasicuaca_kompas.ui.viewmodel.MainViewModel
import com.samsulbahri.aplikasicuaca_kompas.ui.viewmodel.MainViewModelFactory
import com.samsulbahri.aplikasicuaca_kompas.utils.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainListener {
    private val isDatabaseLoaded = MutableLiveData<Boolean>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        val api = WeatherApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val repository = WeatherRepository(api, db)
        val factory = MainViewModelFactory(repository)

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val viewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)

        binding.viewmodel = viewModel

        viewModel.mainListener = this

        if(databaseFileExists(this)) {
            viewModel.getDataBaseWeather().observe(this, { base ->
                if(base != null) {
                    txt_location.text = getString(R.string.location, base.name, base.country)
                    txt_date.text = getString(R.string.updated_at, base.date_time?.toInt()?.unixToDateTime())

                    txt_humidity.text = base.humidity
                    txt_pressure.text = base.pressure
                    txt_temperature.text = getString(R.string.temperature, base.temp?.toDouble()?.kelvinToCelsius())
                    txt_max_temp.text = getString(R.string.max_temperature, base.temp_max?.toDouble()?.kelvinToCelsius())
                    txt_min_temp.text = getString(R.string.min_temperature, base.temp_min?.toDouble()?.kelvinToCelsius())

                    txt_sunrise.text = base.sunrise?.toInt()?.unixToTime()
                    txt_sunset.text = base.sunset?.toInt()?.unixToTime()

                    txt_weather.text = base.main

                    txt_wind.text = base.speed

                    isDatabaseLoaded.value = true
                }
            })
        } else {
            viewModel.onDataLoad(false)
        }

        isDatabaseLoaded.observeOnce(this, {
            if(isInternetAvailable(this)) {
                if(it) {
                    viewModel.onDataLoad(true)
                }
            } else {
                onNoInternet("Pastikan anda memiliki koneksi Internet untuk mendapatkan data terbaru", true)
            }
        })

        srl_refresh.setOnRefreshListener {
            if(isInternetAvailable(this)) {
                if(databaseFileExists(this)) {
                    viewModel.onDataLoad(true)
                } else {
                    viewModel.onDataLoad(false)
                }
            } else {
                onNoInternet("Pastikan anda memiliki koneksi Internet untuk mendapatkan data terbaru", true)
            }

            srl_refresh.isRefreshing = false
        }

    }

    override fun onStarted() {
        pb_loading.visibility = View.VISIBLE
    }

    override fun onSuccess(base: Base) {
        cl_weather.visibility = View.VISIBLE
        ll_no_internet.visibility = View.GONE
        pb_loading.visibility = View.GONE

        txt_location.text = getString(R.string.location, base.name, base.country)
        txt_date.text = getString(R.string.updated_at, base.date_time?.toInt()?.unixToDateTime())

        txt_humidity.text = base.humidity
        txt_pressure.text = base.pressure
        txt_temperature.text = getString(R.string.temperature, base.temp?.toDouble()?.kelvinToCelsius())
        txt_max_temp.text = getString(R.string.max_temperature, base.temp_max?.toDouble()?.kelvinToCelsius())
        txt_min_temp.text = getString(R.string.min_temperature, base.temp_min?.toDouble()?.kelvinToCelsius())

        txt_sunrise.text = base.sunrise?.toInt()?.unixToTime()
        txt_sunset.text = base.sunset?.toInt()?.unixToTime()

        txt_weather.text = base.main

        txt_wind.text = base.speed
    }

    override fun onFailure(message: String) {
        pb_loading.visibility = View.GONE
        toast(message)
    }

    override fun onNoInternet(message: String, isRefresh: Boolean) {
        if(!isRefresh) {
            ll_no_internet.visibility = View.VISIBLE
            cl_weather.visibility = View.GONE
        } else {
            toast(message)
        }
        pb_loading.visibility = View.GONE
    }
}