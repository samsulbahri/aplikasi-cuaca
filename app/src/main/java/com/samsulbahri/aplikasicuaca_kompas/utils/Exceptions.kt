package com.samsulbahri.aplikasicuaca_kompas.utils

import java.io.IOException

class ApiException(message: String): IOException(message)

class NoInternetException(message: String): IOException(message)